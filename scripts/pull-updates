#!/bin/sh

if [ -z "$1" ]; then
	echo "Usage: pull-updates keyring [dir | keyring]" >&2
	exit 1
fi

# avoid gnupg touching ~/.gnupg
GNUPGHOME=$(mktemp -d -t jetring.XXXXXXXX)
export GNUPGHOME
cat > "$GNUPGHOME"/gpg.conf <<EOF
keyid-format 0xlong
import-options import-clean,merge-only
export-options export-clean,no-export-attributes
no-auto-check-trustdb
no-autostart
quiet
EOF
trap cleanup exit
cleanup () {
	rm -rf "$GNUPGHOME"
}

if [ ! -e output/keyrings/debian-keyring.gpg ]; then
	echo "Keyrings don't appear to be built. Run make?"
	exit 1
fi

# Build a set of keyrings
cat output/keyrings/debian-keyring.gpg output/keyrings/debian-nonupload.gpg \
	output/keyrings/debian-maintainers.gpg > $GNUPGHOME/pubring.gpg

mkdir updates/
if [ ! -z "$2" -a -d "$2" ]; then
	# Old style with directory as second parameter
	scripts/explode-keyring $1 updates
else
	# New style. Keyrings all the way.
	touch update-keyring.gpg
	echo Exploding keyrings
	for keyring in $*; do
		scripts/explode-keyring $keyring updates
		cd updates
		for i in 0x*; do
			if [ ! -e ../debian-*-gpg/$i ]; then
				echo $i no longer exists, removing.
				rm $i
			elif cmp -s $i ../debian-*-gpg/$i; then
				echo $i matches old key version, removing.
				rm $i
			fi
		done
		cat 0x* >> ../update-keyring.gpg
		rm 0x*
		cd ..
	done
	echo Importing updates
	gpg --import update-keyring.gpg
	echo Exploding keyring
	for key in $(gpg --list-keys --with-colons < update-keyring.gpg | awk -F: '/^pub/ {print $5}'); do
		gpg --export 0x$key > updates/0x$key
	done
	rm update-keyring.gpg
fi

cd updates
for i in 0x*; do
	if [ ! -e ../debian-*-gpg/$i ]; then
		echo $i no longer exists, removing.
		rm $i
	elif cmp -s $i ../debian-*-gpg/$i; then
		echo $i matches old key version, removing.
		rm $i
	fi
done

echo Updated keys are:
ls

cd ..

for i in updates/0x*; do
	if [ -f $i ]; then
		scripts/update-key --no-clean $i \
			$(dirname debian-*-gpg/$(basename $i))
		rm $i
	fi
done

rmdir updates/
